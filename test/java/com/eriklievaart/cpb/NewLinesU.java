package com.eriklievaart.cpb;

import org.junit.Test;

import com.eriklievaart.toolkit.lang.check.Check;

public class NewLinesU {

	@Test
	public void escapeNothing() {
		Check.isEqual(NewLines.escape("test"), "test");
	}

	@Test
	public void escapeR() {
		Check.isEqual(NewLines.escape("test\r"), "test\\r");
	}

	@Test
	public void escapeN() {
		Check.isEqual(NewLines.escape("test\n"), "test\\n");
	}

	@Test
	public void escapeRN() {
		Check.isEqual(NewLines.escape("test\r\n"), "test\\r\\n");
	}

	@Test
	public void unescapeRN() {
		String input = "test\r\n";
		String escapeAndBack = NewLines.unescape(NewLines.escape(input));
		Check.isEqual(escapeAndBack, input);
	}
}
