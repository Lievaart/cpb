package boot;

import org.junit.Test;

import com.eriklievaart.cpb.SystemClipboardBuffer;
import com.eriklievaart.toolkit.io.SystemClipboard;
import com.eriklievaart.toolkit.lang.check.Check;

public class MainTest {

	@Test
	public void keepAliveUntilClipboardChangesLinux() throws InterruptedException {
		System.setProperty("os.name", "linux");
		SystemClipboardBuffer.writeString("keep program alive");

		Thread thread = new Thread(() -> {
			try {
				Main.persistClipboard();

			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		thread.start();

		Thread.sleep(50);
		Check.isTrue(thread.isAlive());

		SystemClipboard.writeString("changed");
		Thread.sleep(100);
		Check.isFalse(thread.isAlive());
	}

	@Test
	public void keepAliveUntilClipboardChangesEmptyBuffer() throws InterruptedException {
		System.setProperty("os.name", "linux");
		SystemClipboardBuffer.writeString(null);

		long start = System.currentTimeMillis();
		// should exit immediately, since clipboard is empty.
		Main.persistClipboard();

		long spent = System.currentTimeMillis() - start;
		Check.isTrue(spent < 100, "this took $ms", spent);
	}

	@Test
	public void keepAliveUntilClipboardChangesWindows() throws InterruptedException {
		long start = System.currentTimeMillis();
		System.setProperty("os.name", "windows");
		SystemClipboardBuffer.writeString("buffer");

		// should exit immediately on windows, since clipboard is persistent by default.
		Main.persistClipboard();
		Check.isTrue(System.currentTimeMillis() - start < 100);
	}
}
