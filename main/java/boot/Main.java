package boot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;

import com.eriklievaart.cpb.ClipboardManager;
import com.eriklievaart.cpb.Command;
import com.eriklievaart.cpb.CommandParser;
import com.eriklievaart.cpb.SystemClipboardBuffer;
import com.eriklievaart.toolkit.io.SystemClipboard;

public class Main {

	private static final CommandParser PARSER = new CommandParser();

	public static void main(String[] args) throws Exception {
		System.out.println("\n## Copy paste buffer manager ##");

		if (args.length > 0) {
			ClipboardManager.run(PARSER.parse(String.join(" ", args)));
			persistClipboard();
			return;
		}

		while (true) {
			handleUserInput();
		}
	}

	private static void handleUserInput() throws IOException {
		IOUtils.copy(Main.class.getResourceAsStream("/doc/commandline.txt"), System.out);
		dumpClipboard();

		String input = new BufferedReader(new InputStreamReader(System.in)).readLine();
		if (input == null) {
			System.out.println();
			System.exit(0);
		}

		Command command = PARSER.parse(input);
		ClipboardManager.run(command);
	}

	static void persistClipboard() throws InterruptedException {
		// fucking linux clears the clipboard on program exit, so keep program alive until clipboard is modified.
		if (isLinux()) {
			while (SystemClipboardBuffer.clipboardContainsBuffer()) {
				Thread.sleep(100);
			}
		}
	}

	private static boolean isLinux() {
		return System.getProperty("os.name").toLowerCase().contains("linux");
	}

	private static void dumpClipboard() {
		System.out.println();
		System.out.println("on clipboard:");
		System.out.println("--------------------------------------------------------------------------------");
		System.out.println(SystemClipboard.readString());
		System.out.println("--------------------------------------------------------------------------------");
		System.out.print("$");
	}
}
