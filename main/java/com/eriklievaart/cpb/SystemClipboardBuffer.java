package com.eriklievaart.cpb;

import com.eriklievaart.toolkit.io.SystemClipboard;

public class SystemClipboardBuffer {
	private static String buffer = null;

	public static String readString() {
		return SystemClipboard.readString();
	}

	public static void writeString(String data) {
		buffer = data;
		SystemClipboard.writeString(data);
		SystemClipboard.writeSelection(data);
	}

	public static boolean clipboardContainsBuffer() {
		return buffer != null && buffer.equals(readString());
	}
}
