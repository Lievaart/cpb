package com.eriklievaart.cpb;

public class CommandParser {

	public Command parse(String input) {
		String[] headTail = input.trim().split(" ", 2);
		String head = headTail[0];
		String tail = headTail.length == 1 ? "" : headTail[1];

		Command command = new Command();
		command.setHead(head);
		command.setTail(tail);
		return command;
	}
}
