package com.eriklievaart.cpb;

import java.util.List;

import com.eriklievaart.toolkit.io.Console;
import com.eriklievaart.toolkit.lang.collection.NewCollection;

public class ClipboardManager {

	private static List<CommandMethod> index = NewCollection.list();

	static {
		index.add(new CommandMethod("append", CommandExecutor::append));
		index.add(new CommandMethod("read", CommandExecutor::read));
		index.add(new CommandMethod("write", CommandExecutor::write));
		index.add(new CommandMethod("set", CommandExecutor::set));
		index.add(new CommandMethod("exit", CommandExecutor::exit));
		index.add(new CommandMethod("quit", CommandExecutor::exit));
		index.add(new CommandMethod("get", CommandExecutor::get));
		index.add(new CommandMethod("<", CommandExecutor::get));
		index.add(new CommandMethod("post", CommandExecutor::post));
		index.add(new CommandMethod(">", CommandExecutor::post));
		index.add(new CommandMethod("/", CommandExecutor::slash));
		index.add(new CommandMethod("list", CommandExecutor::list));
		index.add(new CommandMethod("+", CommandExecutor::push));
		index.add(new CommandMethod("-", CommandExecutor::pop));
	}

	public static void run(Command command) {
		for (CommandMethod method : index) {
			if (method.getName().startsWith(command.getHead())) {
				method.getConsumer().consume(command);
				return;
			}
		}
		Console.println("## Unknown command %! ##", command.getHead());
	}
}
