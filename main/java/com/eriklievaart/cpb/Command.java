package com.eriklievaart.cpb;

public class Command {

	private String head;
	private String tail;
	
	public String getHead() {
		return head;
	}
	public void setHead(String value) {
		this.head = value;
	}
	public String getTail() {
		return tail;
	}
	public void setTail(String tail) {
		this.tail = tail;
	}
}
