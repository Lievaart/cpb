package com.eriklievaart.cpb;

public interface CommandConsumer {

	void consume(Command command);
}
