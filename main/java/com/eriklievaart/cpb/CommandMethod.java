package com.eriklievaart.cpb;

public class CommandMethod {

	private final String name;
	private final CommandConsumer consumer;

	public CommandMethod(String name, CommandConsumer consumer) {
		this.consumer = consumer;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public CommandConsumer getConsumer() {
		return consumer;
	}
}