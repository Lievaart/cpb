package com.eriklievaart.cpb;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.eriklievaart.io.CommandIO;
import com.eriklievaart.io.HttpRequest;
import com.eriklievaart.io.SettingsIO;
import com.eriklievaart.toolkit.io.Console;
import com.eriklievaart.toolkit.io.FileTool;
import com.eriklievaart.toolkit.io.JvmPaths;
import com.eriklievaart.toolkit.io.RuntimeIOException;
import com.eriklievaart.toolkit.io.UrlTool;
import com.eriklievaart.toolkit.lang.collection.MapTool;
import com.eriklievaart.toolkit.lang.str.Str;

public class CommandExecutor {
	private static final String DEFAULT_CLIPBOARD = "default";
	private static final String CLIPBOARD_URL = SettingsIO.getClipboardUrl();

	public static void get(Command command) {
		try {
			if (Str.isBlank(CLIPBOARD_URL)) {
				throw new IOException("CLIPBOARD_URL unavailable!");
			}
			String name = Str.isBlank(command.getTail()) ? DEFAULT_CLIPBOARD : command.getTail().trim();
			String url = Str.sub("$?name=$", CLIPBOARD_URL, name);
			String data = HttpRequest.get(url);
			if (Str.isBlank(data)) {
				Console.println("*warning* no data for url: " + url);
			} else {
				SystemClipboardBuffer.writeString(data);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void post(Command command) {
		try {
			if (Str.isBlank(CLIPBOARD_URL)) {
				throw new IOException("CLIPBOARD_URL unavailable!");
			}
			String name = Str.isBlank(command.getTail()) ? DEFAULT_CLIPBOARD : command.getTail().trim();
			Console.println("posting to url: " + CLIPBOARD_URL);
			String data = SystemClipboardBuffer.readString();
			if (Str.isBlank(data)) {
				Console.println("*warning* no data to post!");
			} else {
				HttpRequest.post(CLIPBOARD_URL, MapTool.of("name", name, "data", data));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void read(Command command) {
		try {
			SystemClipboardBuffer.writeString(CommandIO.load(command.getTail()));
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("cannot read file: " + command.getTail());
		}
	}

	public static void write(Command command) {
		try {
			CommandIO.store(command.getTail(), SystemClipboardBuffer.readString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void exit(Command command) {
		System.exit(0);
	}

	public static void slash(Command command) {
		SystemClipboardBuffer.writeString(SystemClipboardBuffer.readString().replace('\\', '/'));
	}

	public static void set(Command command) {
		SystemClipboardBuffer.writeString(command.getTail());
	}

	public static void append(Command command) {
		SystemClipboardBuffer.writeString(SystemClipboardBuffer.readString() + command.getTail());
	}

	public static void list(Command command) {
		String[] stored = CommandIO.list();
		Arrays.sort(stored);
		for (String entry : stored) {
			Console.println("\t\t" + entry);
		}
	}

	public static void push(Command command) {
		File file = getStackFile();

		try (FileWriter writer = new FileWriter(file, true)) {

			writer.append(NewLines.escape(SystemClipboardBuffer.readString()) + "\n");

		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}

	public static void pop(Command command) {
		try {
			File file = getStackFile();
			List<String> lines = FileTool.readLines(file);

			if (lines.isEmpty()) {
				System.err.println("Stack is empty!");
				SystemClipboardBuffer.writeString(null);
			} else {
				String pop = lines.remove(lines.size() - 1);
				SystemClipboardBuffer.writeString(NewLines.unescape(pop));
				FileTool.writeLines(file, lines);
			}
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}

	private static File getStackFile() {
		try {
			String root = JvmPaths.getJarDirOrRunDir(CommandExecutor.class);
			File file = new File(UrlTool.append(root, "data/stack.txt"));

			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			return file;
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}
}
