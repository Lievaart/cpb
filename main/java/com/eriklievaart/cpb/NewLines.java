package com.eriklievaart.cpb;

import java.util.Map;

import com.eriklievaart.toolkit.lang.collection.NewCollection;
import com.eriklievaart.toolkit.lang.str.StringEscape;

public class NewLines {

	private static Map<Character, Character> mapping = NewCollection.map();

	static {
		mapping.put('b', '\\');
		mapping.put('r', '\r');
		mapping.put('n', '\n');
	}

	public static String escape(String in) {
		return new StringEscape(mapping).escape(in);
	}

	public static String unescape(String in) {
		return new StringEscape(mapping).unescape(in);
	}

}
