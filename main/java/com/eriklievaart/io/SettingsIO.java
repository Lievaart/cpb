package com.eriklievaart.io;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.eriklievaart.toolkit.io.JvmPaths;
import com.eriklievaart.toolkit.io.PropertiesIO;
import com.eriklievaart.toolkit.lang.LogTemplate;

public class SettingsIO {
	private static final LogTemplate log = new LogTemplate(SettingsIO.class);

	private static final String CLIPBOARD_URL_PROPERTY = "clipboard.url";
	private static final String JVM_ROOT = JvmPaths.getJarDirOrRunDir(SettingsIO.class);
	private static final File FILE = new File(JVM_ROOT, "data/application.properties");

	public static String getClipboardUrl() {
		try {
			Map<String, String> properties = PropertiesIO.loadStrings(FILE);
			if (properties.containsKey(CLIPBOARD_URL_PROPERTY)) {
				return properties.get(CLIPBOARD_URL_PROPERTY);
			}
		} catch (IOException e) {
			log.warn("File not found: " + FILE);
		}
		log.warn("clipboard.url not configured in " + FILE);
		return null;
	}

}