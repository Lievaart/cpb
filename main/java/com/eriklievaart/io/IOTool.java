package com.eriklievaart.io;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class IOTool {

	public static String toString(InputStream is) {
        try (Scanner scanner = new Scanner(is)) {
            scanner.useDelimiter("\\A");
            return scanner.hasNext() ? scanner.next() : "";
        }
    }
	
	public static void writeStringToOutputStream(String html, OutputStream os) {
        try (PrintWriter writer = new PrintWriter(os)) {
            writer.write(html);
        }
    }
}
