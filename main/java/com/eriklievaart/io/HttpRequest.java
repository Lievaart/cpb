package com.eriklievaart.io;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.eriklievaart.toolkit.lang.LogTemplate;
import com.eriklievaart.toolkit.lang.check.Check;
import com.eriklievaart.toolkit.lang.collection.NewCollection;

public class HttpRequest {
	private static LogTemplate log = new LogTemplate(HttpRequest.class);

	public static String get(String url) throws IOException {
		Check.notNull(url);
		HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
		con.setRequestMethod("GET");
		log.debug("GET $ => $", url, con.getResponseCode());

		return IOTool.toString(con.getInputStream());
	}

	public static String post(String url, Map<String, String> params) throws IOException {
		Check.notNull(url, params);
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("POST");

		String urlParameters = paramsToString(params);

		con.setDoOutput(true);
		IOTool.writeStringToOutputStream(urlParameters, con.getOutputStream());
		log.debug("POST $ => $", url, con.getResponseCode());

		return IOTool.toString(con.getInputStream());
	}

	private static String paramsToString(Map<String, String> params) throws UnsupportedEncodingException {
		List<String> list = NewCollection.list();
		for (Entry<String, String> entry : params.entrySet()) {
			list.add(entry.getKey() + "=" + URLEncoder.encode(entry.getValue(), "UTF-8"));
		}
		return String.join("&", list);
	}
}
