package com.eriklievaart.io;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.eriklievaart.toolkit.io.JvmPaths;
import com.eriklievaart.toolkit.lang.str.Str;

public class CommandIO {

	private static final File ROOT = new File(JvmPaths.getJarDirOrRunDir(CommandIO.class), "data/bookmarks");

	public static String load(String raw) throws IOException {
		return FileUtils.readFileToString(getFile(raw));
	}

	public static void store(String raw, String buffer) throws IOException {
		FileUtils.writeStringToFile(getFile(raw), buffer);
	}

	private static File getFile(String raw) throws IOException {
		if (Str.isBlank(raw)) {
			throw new IOException("File name is blank");
		}
		return new File(ROOT, raw.replaceAll("\\W", ""));
	}

	public static String[] list() {
		return ROOT.list();
	}
}
